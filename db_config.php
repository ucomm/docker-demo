<?php

// hold credentials and create a connection to the db.

$servername = "mysql";
$username = "wordpress";
$password = "wordpress";
$dbname = "wordpress";

// connect to the database with the credentials
$connection = new mysqli($servername, $username, $password, $dbname);
if ($connection->connect_error) {
  die("Connection failed: " . $connection->connect_error);
}