<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Hello App!</title>
</head>
<body>

<?php
// include the db configuration (username, password, db name, etc)
require('db_config.php');

// validate form submission (just checking if empty) and insert into db.
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
  
  // validating
  $errors = array();

  if (empty($_POST['firstname'])) {
    $errors['firstname'] = 'No first name';
  } 
  if (empty($_POST['lastname'])) {
    $errors['lastname'] = 'No last name';
  } 
  if (empty($_POST['email'])) {
    $errors['email'] = 'No email';
  }

  // insert into db if no errors
  if (empty($errors)) {
    $sql = "
      INSERT INTO MyContacts (
        firstname, lastname, email
      ) VALUES (
        '$_POST[firstname]', '$_POST[lastname]', '$_POST[email]'
      )
    ";
    if ($connection->query($sql) === true) {
      $message = "New contact inserted";
    }
  }
}

// select all contacts from the table
$query = "SELECT * FROM MyContacts";
$result = $connection->query($query);

$contacts_array = array();
if ($result->num_rows > 0) {
  while ($row = $result->fetch_assoc()) {
    array_push($contacts_array, $row);
  }
}

?>
<h1>A Form!</h1>

<?php
  // print errors
  if (!empty($errors)) {
?>
  <p>There were errors</p>
  <ul>
  <?php
    foreach ($errors as $key => $error) {
  ?>
    <li><?php echo $error; ?></li>
  <?php   
    }
  ?>
  </ul>
<?php
  } elseif (!empty($message)) {
?>
    <p><?php echo $message; ?></p>
<?php
  }
?>

<form method='POST' action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>">
  <label for="firstname">First Name</label>
  <input id="firstname" name="firstname"  type="text" value="<?php echo $first_name ?>" />
  <label for="lastname">Last Name</label>
  <input id="lastname" name="lastname" type="text" value="<?php echo $last_name ?>" />
  <label  for="email">Email</label>
  <input id="email" name="email" type="email" value="<?php echo $email ?>" />
  <br /><br />
  <input type="submit" name="submit" value="Submit" />
</form>
<h2>Your contacts</h2>
<table>
  <tr>
    <th>First Name</th>
    <th>Last Name</th>
    <th>Email</th>
  </tr>
  <?php
    // print the contact records to a table.
    if (!empty($contacts_array)) {
      foreach ($contacts_array as $contact) {
  ?>
        <tr>
          <td><?php echo $contact['firstname']; ?></td>
          <td><?php echo $contact['lastname']; ?></td>
          <td><?php echo $contact['email']; ?></td>
        </tr>
  <?php
      }
    }
  ?>
</table>
</body>
</html>
