<?php
// Run this file first from the terminal `php setup.php`
// include the db configuration (username, password, db name, etc)
require('db_config.php');

$sql = "
  CREATE TABLE MyContacts (
    id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    firstname VARCHAR(30) NOT NULL,
    lastname VARCHAR(30) NOT NULL,
    email VARCHAR(50),
    reg_date TIMESTAMP
  )
";

if ($connection->query($sql) === TRUE) {
  echo "Table MyContacts created sucessfully";
} else {
  echo "Error creating table: " . $connection->error;
}
