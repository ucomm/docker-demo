# Docker demo
## Getting started
### Installing Docker
To install docker, visit the install docs and follow the instructions for either [macOS](https://docs.docker.com/docker-for-mac/install/) or [Windows](https://docs.docker.com/docker-for-windows/install/). To install docker in Ubuntu, you can follow [this DigitalOcean tutorial](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-16-04). 

Once Docker is installed, you should have access to two terminal commands.
- `docker`
- `docker-compose`
You will also have access to [Docker swarm](https://docs.docker.com/engine/swarm/) (for container orchestration), but that's beyond the scope of this project.

To ensure that docker and docker-compose are running correctly you can use `docker -v` or `docker-compose -v`.

### Hello world
Docker uses a system of images and containers to organize and coordinate operations. Images are similar to classes in an OOP style programming language and are organized inside Dockerfiles.
Images have several characteristics
- They have a list of instructions to be executed every time they are spun up into a container
- They can inherit from other images
- They are static (with exceptions for mounting volumes inside a running container).
- They should be responsible for only one process or operation.

Containers are similar to instances of an object in OOP. 
- They will initially run the instructions from the image they're built from (although this can be overriden to an extent).
- They can maintain state even after being shut down.
- They can mount files from the host and perform actions on them.
- They can be combined with other containers to create complex applications.

To run your first container and see the results, simply run `docker run hello-world`. This will do things
- If no hello-world image exists locally, it will be fetched from the docker hub.
- It will then spin up a container based on that image and execute the commands inside.
- Once finished, it will stop the container from running.

Alternatively if you don't want to immediately run the container, you can choose to do `docker pull hello-world` and then run it as above.

Here are some additional commands that are useful at this stage
- `docker ps` - this will list running containers
  - `docker ps -l` - list the last container run
  - `docker ps -a` - list all containers
- `docker rm {container name/id}` - destroy the referenced container
- `docker images` - check which images are available
- `docker rmi -f {image name/id}` - remove the referenced image

## Creating a Containerized Linux Server
Now we'll try running a Centos server inside a container based on our UComm comm0-ssl image. This will demonstrate
- naming a container
- binding a port
- mounting a volume
- removing the container when it exits
- exploring the container via bash
- overriding the default image command and entering directly into bash

docker commands typically take the format of `docker {command} [arguments/flags] image-name (command to run on the container)`.

### Pulling the image and creating a container
Similar to the hello world example, you can pull our comm0-ssl image by running `docker pull uconn/comm0-ssl` and ensuring that it has been pulled by checking `docker images`. 

### Starting the container
Next start the server and tag it `hello-server` by running - 

`docker run --name hello-server uconn/comm0-ssl`. 

This will start a container and you will see that it is running. To inspect the container run `docker ps`. Now you will see that there are two ports available (80 and 443) as well as other information. You can now stop the container and remove it to continue to the next part.
- `docker stop {container name or id}`
- `docker rm {container name or id}`

If you have multiple containers running `docker stop $(docker ps -q)` is a useful command to stop them all gracefully. 

**NB - Typically we don't remove containers for projects since we want to preserve their state. This is just for practice**

### Binding ports from the host to the container
Next we want to bind a port from the host to the container. With the container stopped, add the flag `-p 80:80` to the previous command. If you don't want to keep stoping and then removing the container separately, you can also pass the `--rm` flag to the command. It should look like

`docker run --name hello-server -p 80:80 --rm uconn/comm0-ssl`

If you inspect the running containers again, you'll see that port 80 is bound from the host to the container. You can confirm this by pinging localhost.

Stop and remove the container as above to continue.

### Mounting a volume.
The server is ready to host our project. To mount a volume, we simply pass the `-v` flag with the path to the files and the path to the right directory in the server. You can also interact directly with the container to stop it by passing the `-it` flags.

`docker run --name hello-server -p 80:80 -v {your path to this project}:/var/www/html --rm -it uconn/comm0-ssl`

If you visit localhost now, you should see the project being served and you can interact with it.

### Exploring the Continainer via bash
Somtimes it's useful to get inside a container. For instance to read error logs or make sure that files are mounted correctly. This is easily done. In a separate terminal window, run the following - 

`docker exec -it {container name/id} /bin/bash`

This will automatically send you into the container as root.

### Overriding the Default Image Command
Up to this point, we've been working with the default image command which automatically runs the apache server. If (for some reason) you wanted to enter bash right away in the container instead of starting the server you could do the following - 

`docker run --name hello-server -it uconn/comm0-ssl /bin/bash`

However, this container wouldn't be connected to anything on the host. Even if you passed the port and volume arguments, it still wouldn't serve any files. The `/bin/bash` command replaces the default.

## Docker Compose
While it is certainly possible to coordinate multiple containers from the docker cli, it's kind of a pain. Fortunately that's where the `docker-compose.yml` file comes in. If we took all the things we did above and organize them into the file it would look like this
```yml
server:
  image: uconn/comm0-ssl:latest
  ports:
    - "80:80"
    - "443:443"
  volumes:
    - ./:/var/www/html
```
From here all you need to do is run `docker-compose up` (cntrl-C will stop the container). If you uncomment the included docker-compose file, and run `docker-compose up`, you should be able to see two running containers (`docker ps`)one for a server and one for a database. Note that they are automatically named with the following syntax `current-directory_service_instance`.

In the docker-compose file the `links` key binds the two services together.